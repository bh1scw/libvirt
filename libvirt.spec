%define arches_qemu_kvm         x86_64 ppc64le aarch64
%define arches_64bit            x86_64 ppc64le aarch64 riscv64
%define arches_x86              x86_64
%define arches_systemtap_64bit  %{arches_64bit}
%define arches_dmidecode        %{arches_x86}
%define arches_xen              %{arches_x86} aarch64
%define arches_vbox             %{arches_x86}
%define arches_ceph             %{arches_64bit}
%define arches_zfs              %{arches_x86} ppc64le 
%define arches_numactl          %{arches_x86} ppc64le aarch64
%define arches_numad            %{arches_x86} ppc64le aarch64

%bcond_without qemu        
%bcond_without lxc         
%bcond_with libxl       
%bcond_with vbox       

%ifarch %{arches_qemu_kvm}
    %if %{with qemu}
        %bcond_without qemu_kvm
    %else
        %bcond_with qemu_kvm
    %endif
%else
    %bcond_with qemu_kvm
%endif

%if %{with qemu}
    %bcond_without qemu_tcg
%else
    %bcond_with qemu_tcg
%endif



%if ! %{with qemu_tcg} && ! %{with qemu_kvm}
    %bcond_with qemu
%endif
                    
%bcond_without esx 
%bcond_with storage_zfs  
%bcond_without numactl
%bcond_without sanlock                     
%bcond_with storage_rbd      
%bcond_with storage_gluster
%bcond_with storage_iscsi_direct
%bcond_with fuse       
%bcond_with numad 
%bcond_with netcf 
%bcond_with wireshark   
%bcond_with dmidecode

%ifnarch %{arches_numactl}
    %bcond_with numactl
%endif
%ifnarch %{arches_ceph}
    %bcond_with storage_rbd
%endif

%bcond_with openvz
%bcond_with vmware
%bcond_with hyperv
%bcond_with vz

%bcond_without firewalld_zone

%if %{with lxc}
    %bcond_without fuse
%endif


%bcond_without libssh2

%define wireshark_plugindir %(pkg-config --variable plugindir wireshark)/epan

%bcond_without libssh

%if %{with qemu} || %{with lxc}
    %ifarch %{arches_numad}
        %bcond_without numad
    %endif
%endif

%ifarch %{arches_dmidecode}
    %bcond_without dmidecode
%endif

%bcond_without modular_daemons
%define qemu_user  qemu
%define qemu_group  qemu
%define qemu_moddir %{_libdir}/qemu
%define qemu_datadir %{_datadir}/qemu

%bcond_without mingw
%define enable_werror -Dwerror=true
%define tls_priority "@LIBVIRT,SYSTEM"
%define libvirt_sysconfig_pre() \
    for sc in %{?*} ; do \
        test -f "%{_sysconfdir}/sysconfig/${sc}.rpmsave" || continue ; \
        mv -v "%{_sysconfdir}/sysconfig/${sc}.rpmsave" "%{_sysconfdir}/sysconfig/${sc}.rpmsave.old" ; \
    done \
    %{nil}
%define libvirt_sysconfig_posttrans() \
    for sc in %{?*} ; do \
        test -f "%{_sysconfdir}/sysconfig/${sc}.rpmsave" || continue ; \
        mv -v "%{_sysconfdir}/sysconfig/${sc}.rpmsave" "%{_sysconfdir}/sysconfig/${sc}" ; \
    done \
    %{nil}



Summary:      Library providing a simple virtualization API
Name:         libvirt
Version:      9.6.0
Release:      8%{?dist}
License:      LGPLv2+
URL:          https://libvirt.org/
Source0:      https://libvirt.org/sources/libvirt-%{version}.tar.xz

Requires:     libvirt-daemon = %{version}-%{release}
Requires:     libvirt-daemon-config-network = %{version}-%{release}
Requires:     libvirt-daemon-config-nwfilter = %{version}-%{release}
%if %{with libxl}
Requires:     libvirt-daemon-driver-libxl = %{version}-%{release}
%endif
%if %{with lxc}
Requires:     libvirt-daemon-driver-lxc = %{version}-%{release}
%endif
%if %{with qemu}
Requires:     libvirt-daemon-driver-qemu = %{version}-%{release}
%endif

%if %{with vbox}
Requires:     libvirt-daemon-driver-vbox = %{version}-%{release}
%endif
Requires:     libvirt-daemon-driver-nwfilter = %{version}-%{release}
Requires:     libvirt-daemon-driver-interface = %{version}-%{release}
Requires:     libvirt-daemon-driver-secret = %{version}-%{release}
Requires:     libvirt-daemon-driver-storage = %{version}-%{release}
Requires:     libvirt-daemon-driver-network = %{version}-%{release}
Requires:     libvirt-daemon-driver-nodedev = %{version}-%{release}
Requires:     libvirt-client = %{version}-%{release}
Requires:     libvirt-libs = %{version}-%{release}

BuildRequires: python3-docutils
BuildRequires: gcc meson >= 0.56.0
BuildRequires: ninja-build git
BuildRequires: perl-interpreter python3
%if %{with libxl}
BuildRequires: xen-devel
%endif
BuildRequires: glib2-devel >= 2.56 libxml2-devel
BuildRequires: libxslt readline-devel bash-completion >= 2.0
BuildRequires: gettext libtasn1-devel gnutls-devel libattr-devel
BuildRequires: libblkid-devel >= 2.17 augeas
BuildRequires: systemd-devel >= 185 systemd-rpm-macros
BuildRequires: libpciaccess-devel >= 0.10.9 yajl-devel
%if %{with sanlock}
BuildRequires: sanlock-devel >= 2.4
%endif
BuildRequires: libpcap-devel >= 1.5.0
BuildRequires: libnl3-devel libselinux-devel
BuildRequires: iptables ebtables
BuildRequires: module-init-tools cyrus-sasl-devel
BuildRequires: polkit >= 0.112 util-linux
%if %{with qemu}
BuildRequires: libacl-devel qemu-kvm
%endif
BuildRequires: lvm2 iscsi-initiator-utils
%if %{with storage_iscsi_direct}
BuildRequires: libiscsi-devel
%endif
BuildRequires: parted-devel device-mapper-devel
%if %{with storage_rbd}
BuildRequires: librados-devel librbd-devel
%endif
%if %{with storage_gluster}
BuildRequires: glusterfs-api-devel >= 3.4.1 glusterfs-devel >= 3.4.1
%endif
%if %{with numactl}
BuildRequires: numactl-devel
%endif
BuildRequires: libcap-ng-devel >= 0.5.0
%if %{with fuse}
BuildRequires: fuse-devel >= 2.8.6
%endif
%if %{with libssh2}
BuildRequires: libssh2-devel >= 1.3.0
%endif
%if %{with netcf}
BuildRequires: netcf-devel >= 0.2.2
%endif
%if %{with esx}
BuildRequires: libcurl-devel
%endif
%if %{with hyperv}
BuildRequires: libwsman-devel >= 2.6.3
%endif
BuildRequires: audit-libs-devel systemtap-sdt-devel
BuildRequires: util-linux nfs-utils
BuildRequires: gawk scrub

%if %{with numad}
BuildRequires: numad
%endif

%if %{with wireshark}
BuildRequires: wireshark-devel
%endif

%if %{with libssh}
BuildRequires: libssh-devel >= 0.8.1
%endif

BuildRequires: rpcgen
BuildRequires: libtirpc-devel

%if %{with firewalld_zone}
BuildRequires: firewalld-filesystem
%endif

%if %{with mingw}
BuildRequires: mingw32-filesystem mingw32-gcc mingw32-binutils
BuildRequires: mingw32-glib2 >= 2.48 mingw32-libgpg-error mingw32-libgcrypt
BuildRequires: mingw32-gnutls mingw32-gettext mingw32-libxml2
BuildRequires: mingw32-portablexdr mingw32-dlfcn
BuildRequires: mingw32-libssh2mingw32-curl mingw64-filesystem
BuildRequires: mingw64-gcc mingw64-binutils
BuildRequires: mingw64-glib2 >= 2.48 mingw64-libgpg-error
BuildRequires: mingw64-libgcrypt mingw64-gnutls
BuildRequires: mingw64-gettext mingw64-libxml2
BuildRequires: mingw64-portablexdr mingw64-dlfcn
BuildRequires: mingw64-libssh2 mingw64-curl
%endif

%description
Libvirt is a C toolkit to interact with the virtualization capabilities
of recent versions of Linux (and other OSes). The main package includes
the libvirtd server exporting the virtualization support.

%package docs
Summary: API reference and website documentation

%description docs
Includes the API reference for the libvirt C library, and a complete
copy of the libvirt.org website documentation.

%package daemon
Summary: Server side daemon and supporting files for libvirt library
Requires: %{name}-libs = %{version}-%{release}
Recommends: %{name}-client = %{version}-%{release}
Recommends: /usr/bin/nc
Requires: module-init-tools
Requires: iproute iproute-tc
Requires: polkit >= 0.112
%if %{with dmidecode}
Requires: dmidecode
%endif
Requires(post): /usr/bin/systemctl
%if %{with numad}
Requires: numad
%endif
Requires: dbus
Requires(pre): shadow-utils
Requires: gettext-runtime
Provides: libvirt-admin = %{version}-%{release}


%description daemon
Server side daemon required to manage the virtualization capabilities
of recent versions of Linux. Requires a hypervisor specific sub-RPM
for specific drivers.

%package daemon-config-network
Summary:  Default configuration files for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-daemon-driver-network = %{version}-%{release}

%description daemon-config-network
Default configuration files for setting up NAT based networking

%package daemon-config-nwfilter
Summary:  Network filter configuration files for the libvirtd daemon

Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-daemon-driver-nwfilter = %{version}-%{release}

%description daemon-config-nwfilter
Network filter configuration files for cleaning guest traffic

%package daemon-driver-network
Summary:  Network driver plugin for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: dnsmasq >= 2.41
Requires: iptables

%description daemon-driver-network
The network driver plugin for the libvirtd daemon, providing
an implementation of the virtual network APIs using the Linux
bridge capabilities.


%package daemon-driver-nwfilter
Summary:  Nwfilter driver plugin for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: iptables
Requires: ebtables

%description daemon-driver-nwfilter
The nwfilter driver plugin for the libvirtd daemon, providing
an implementation of the firewall APIs using the ebtables,
iptables and ip6tables capabilities


%package daemon-driver-nodedev
Summary:  Nodedev driver plugin for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: systemd >= 185
Requires: mdevctl

%description daemon-driver-nodedev
The nodedev driver plugin for the libvirtd daemon, providing
an implementation of the node device APIs using the udev
capabilities.


%package daemon-driver-interface
Summary:  Interface driver plugin for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
%if %{with netcf}
Requires: netcf-libs >= 0.2.2
%endif

%description daemon-driver-interface
The interface driver plugin for the libvirtd daemon, providing
an implementation of the host network interface APIs.

%package daemon-driver-secret
Summary:  Secret driver plugin for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}

%description daemon-driver-secret
The secret driver plugin for the libvirtd daemon, providing
an implementation of the secret key APIs.

%package daemon-driver-storage-core
Summary:  Storage driver plugin including base backends for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: nfs-utils util-linux
%if %{with qemu}
Requires: qemu-kvm
%endif

%description daemon-driver-storage-core
The storage driver plugin for the libvirtd daemon, providing
an implementation of the storage APIs using files, local disks, LVM, SCSI,
iSCSI, and multipath storage.

%package daemon-driver-storage-logical
Summary:  Storage driver plugin for lvm volumes
Requires: libvirt-daemon-driver-storage-core = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: lvm2

%description daemon-driver-storage-logical
The storage driver backend adding implementation of the storage APIs for block
volumes using lvm.


%package daemon-driver-storage-disk
Summary:  Storage driver plugin for disk
Requires: libvirt-daemon-driver-storage-core = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: parted device-mapper

%description daemon-driver-storage-disk
The storage driver backend adding implementation of the storage APIs for block
volumes using the host disks.


%package daemon-driver-storage-scsi
Summary:  Storage driver plugin for local scsi devices
Requires: libvirt-daemon-driver-storage-core = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}

%description daemon-driver-storage-scsi
The storage driver backend adding implementation of the storage APIs for scsi
host devices.


%package daemon-driver-storage-iscsi
Summary:  Storage driver plugin for iscsi
Requires: libvirt-daemon-driver-storage-core = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: iscsi-initiator-utils

%description daemon-driver-storage-iscsi
The storage driver backend adding implementation of the storage APIs for iscsi
volumes using the host iscsi stack.


%if %{with storage_iscsi_direct}
%package daemon-driver-storage-iscsi-direct
Summary:  Storage driver plugin for iscsi-direct
Requires: libvirt-daemon-driver-storage-core = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}

%description daemon-driver-storage-iscsi-direct
The storage driver backend adding implementation of the storage APIs for iscsi
volumes using libiscsi direct connection.
%endif


%package daemon-driver-storage-mpath
Summary:  Storage driver plugin for multipath volumes
Requires: libvirt-daemon-driver-storage-core = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: device-mapper

%description daemon-driver-storage-mpath
The storage driver backend adding implementation of the storage APIs for
multipath storage using device mapper.


%if %{with storage_gluster}
%package daemon-driver-storage-gluster
Summary:  Storage driver plugin for gluster
Requires: libvirt-daemon-driver-storage-core = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: glusterfs-client >= 2.0.1  
Requires: /usr/sbin/gluster


%description daemon-driver-storage-gluster
The storage driver backend adding implementation of the storage APIs for gluster
volumes using libgfapi.
%endif


%if %{with storage_rbd}
%package daemon-driver-storage-rbd
Summary: Storage driver plugin for rbd
Requires: libvirt-daemon-driver-storage-core = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}

%description daemon-driver-storage-rbd
The storage driver backend adding implementation of the storage APIs for rbd
volumes using the ceph protocol.
%endif


%if %{with storage_zfs}
%package daemon-driver-storage-zfs
Summary: Storage driver plugin for ZFS
Requires: libvirt-daemon-driver-storage-core = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: /sbin/zfs /sbin/zpool

%description daemon-driver-storage-zfs
The storage driver backend adding implementation of the storage APIs for
ZFS volumes.
%endif


%package daemon-driver-storage
Summary: Storage driver plugin including all backends for the libvirtd daemon
Requires: libvirt-daemon-driver-storage-core = %{version}-%{release}
Requires: libvirt-daemon-driver-storage-disk = %{version}-%{release}
Requires: libvirt-daemon-driver-storage-logical = %{version}-%{release}
Requires: libvirt-daemon-driver-storage-scsi = %{version}-%{release}
Requires: libvirt-daemon-driver-storage-iscsi = %{version}-%{release}
Requires: libvirt-daemon-driver-storage-mpath = %{version}-%{release}
%if %{with storage_iscsi_direct}
Requires: libvirt-daemon-driver-storage-iscsi-direct = %{version}-%{release}
%endif
%if %{with storage_gluster}
Requires: libvirt-daemon-driver-storage-gluster = %{version}-%{release}
%endif
%if %{with storage_rbd}
Requires: libvirt-daemon-driver-storage-rbd = %{version}-%{release}
%endif
%if %{with storage_zfs}
Requires: libvirt-daemon-driver-storage-zfs = %{version}-%{release}
%endif

%description daemon-driver-storage
The storage driver plugin for the libvirtd daemon, providing
an implementation of the storage APIs using LVM, iSCSI,
parted and more.


%if %{with qemu}
%package daemon-driver-qemu
Summary: QEMU driver plugin for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: qemu-kvm gzip bzip2
Requires: lzop xz
Requires: systemd-container swtpm-tools

%description daemon-driver-qemu
The qemu driver plugin for the libvirtd daemon, providing
an implementation of the hypervisor driver APIs using
QEMU
%endif


%if %{with lxc}
%package daemon-driver-lxc
Summary: LXC driver plugin for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Requires: libvirt-daemon-driver-network = %{version}-%{release}
Requires: systemd-container

%description daemon-driver-lxc
The LXC driver plugin for the libvirtd daemon, providing
an implementation of the hypervisor driver APIs using
the Linux kernel
%endif


%if %{with vbox}
%package daemon-driver-vbox
Summary: VirtualBox driver plugin for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}

%description daemon-driver-vbox
The vbox driver plugin for the libvirtd daemon, providing
an implementation of the hypervisor driver APIs using
VirtualBox
%endif


%if %{with libxl}
%package daemon-driver-libxl
Summary: Libxl driver plugin for the libvirtd daemon
Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-libs = %{version}-%{release}
Obsoletes: libvirt-daemon-driver-xen < 4.3.0

%description daemon-driver-libxl
The Libxl driver plugin for the libvirtd daemon, providing
an implementation of the hypervisor driver APIs using
Libxl
%endif



%if %{with qemu_tcg}
%package daemon-qemu
Summary: Server side daemon & driver required to run QEMU guests

Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-daemon-driver-qemu = %{version}-%{release}
Requires: libvirt-daemon-driver-interface = %{version}-%{release}
Requires: libvirt-daemon-driver-network = %{version}-%{release}
Requires: libvirt-daemon-driver-nodedev = %{version}-%{release}
Requires: libvirt-daemon-driver-nwfilter = %{version}-%{release}
Requires: libvirt-daemon-driver-secret = %{version}-%{release}
Requires: libvirt-daemon-driver-storage = %{version}-%{release}
Requires: qemu

%description daemon-qemu
Server side daemon and driver required to manage the virtualization
capabilities of the QEMU TCG emulators
%endif


%if %{with qemu_kvm}
%package daemon-kvm
Summary: Server side daemon & driver required to run KVM guests

Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-daemon-driver-qemu = %{version}-%{release}
Requires: libvirt-daemon-driver-interface = %{version}-%{release}
Requires: libvirt-daemon-driver-network = %{version}-%{release}
Requires: libvirt-daemon-driver-nodedev = %{version}-%{release}
Requires: libvirt-daemon-driver-nwfilter = %{version}-%{release}
Requires: libvirt-daemon-driver-secret = %{version}-%{release}
Requires: libvirt-daemon-driver-storage = %{version}-%{release}
Requires: qemu-kvm

%description daemon-kvm
Server side daemon and driver required to manage the virtualization
capabilities of the KVM hypervisor
%endif


%if %{with lxc}
%package daemon-lxc
Summary: Server side daemon & driver required to run LXC guests

Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-daemon-driver-lxc = %{version}-%{release}
Requires: libvirt-daemon-driver-interface = %{version}-%{release}
Requires: libvirt-daemon-driver-network = %{version}-%{release}
Requires: libvirt-daemon-driver-nodedev = %{version}-%{release}
Requires: libvirt-daemon-driver-nwfilter = %{version}-%{release}
Requires: libvirt-daemon-driver-secret = %{version}-%{release}
Requires: libvirt-daemon-driver-storage = %{version}-%{release}

%description daemon-lxc
Server side daemon and driver required to manage the virtualization
capabilities of LXC
%endif


%if %{with libxl}
%package daemon-xen
Summary: Server side daemon & driver required to run XEN guests

Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-daemon-driver-libxl = %{version}-%{release}
Requires: libvirt-daemon-driver-interface = %{version}-%{release}
Requires: libvirt-daemon-driver-network = %{version}-%{release}
Requires: libvirt-daemon-driver-nodedev = %{version}-%{release}
Requires: libvirt-daemon-driver-nwfilter = %{version}-%{release}
Requires: libvirt-daemon-driver-secret = %{version}-%{release}
Requires: libvirt-daemon-driver-storage = %{version}-%{release}
Requires: xen

%description daemon-xen
Server side daemon and driver required to manage the virtualization
capabilities of XEN
%endif

%if %{with vbox}
%package daemon-vbox
Summary: Server side daemon & driver required to run VirtualBox guests

Requires: libvirt-daemon = %{version}-%{release}
Requires: libvirt-daemon-driver-vbox = %{version}-%{release}
Requires: libvirt-daemon-driver-interface = %{version}-%{release}
Requires: libvirt-daemon-driver-network = %{version}-%{release}
Requires: libvirt-daemon-driver-nodedev = %{version}-%{release}
Requires: libvirt-daemon-driver-nwfilter = %{version}-%{release}
Requires: libvirt-daemon-driver-secret = %{version}-%{release}
Requires: libvirt-daemon-driver-storage = %{version}-%{release}

%description daemon-vbox
Server side daemon and driver required to manage the virtualization
capabilities of VirtualBox
%endif

%package client
Summary: Client side utilities of the libvirt library
Requires: %{name}-libs = %{version}-%{release}
Requires: gnutls-utils

%description client
The client binaries needed to access the virtualization
capabilities of recent versions of Linux (and other OSes).

%package client-qemu
Summary: Additional client side utilities for QEMU
Requires: %{name}-libs = %{version}-%{release}
Requires: python3-libvirt >= 3.7.0

%description client-qemu
The additional client binaries are used to interact
with some QEMU specific features of libvirt.

%package libs
Summary: Client side libraries
Requires: cyrus-sasl
Requires: cyrus-sasl-gssapi

%description libs
Shared libraries for accessing the libvirt daemon.

%if %{with wireshark}
%package wireshark
Summary: Wireshark dissector plugin for libvirt RPC transactions
Requires: wireshark
Requires: %{name}-libs = %{version}-%{release}

%description wireshark
Wireshark dissector plugin for better analysis of libvirt RPC traffic.
%endif

%if %{with lxc}
%package login-shell
Summary: Login shell for connecting users to an LXC container
Requires: %{name}-libs = %{version}-%{release}

%description login-shell
Provides the set-uid virt-login-shell binary that is used to
connect a user to an LXC container when they login, by switching
namespaces.
%endif

%package devel
Summary: Libraries, includes, etc. to compile with the libvirt library
Requires: %{name}-libs = %{version}-%{release}
Requires: pkgconfig

%description devel
Include header files & development libraries for the libvirt C library.

%if %{with sanlock}
%package lock-sanlock
Summary: Sanlock lock manager plugin for QEMU driver
Requires: sanlock >= 2.4
Requires: augeas
Requires: %{name}-daemon = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}

%description lock-sanlock
Includes the Sanlock lock manager plugin for the QEMU
driver
%endif

%package nss
Summary: Libvirt plugin for Name Service Switch
Requires: libvirt-daemon-driver-network = %{version}-%{release}

%description nss
Libvirt plugin for NSS for translating domain names into IP addresses.

%if %{with mingw}
%package -n mingw32-libvirt
Summary: %{summary}
Obsoletes: mingw32-libvirt-static < 7.0.0
BuildArch: noarch

%description -n mingw32-libvirt
MinGW Windows libvirt virtualization library.

%package -n mingw64-libvirt
Summary: %{summary}
Obsoletes: mingw64-libvirt-static < 7.0.0
BuildArch: noarch

%description -n mingw64-libvirt
MinGW Windows libvirt virtualization library.

%{?mingw_debug_package}
%endif

%prep
%autosetup -S git_am

%build
%define supported_platform 1

%if %{with qemu}
    %define arg_qemu -Ddriver_qemu=enabled
%else
    %define arg_qemu -Ddriver_qemu=disabled
%endif

%if %{with openvz}
    %define arg_openvz -Ddriver_openvz=enabled
%else
    %define arg_openvz -Ddriver_openvz=disabled
%endif

%if %{with lxc}
    %define arg_lxc -Ddriver_lxc=enabled
    %define arg_login_shell -Dlogin_shell=enabled
%else
    %define arg_lxc -Ddriver_lxc=disabled
    %define arg_login_shell -Dlogin_shell=disabled
%endif

%if %{with vbox}
    %define arg_vbox -Ddriver_vbox=enabled
%else
    %define arg_vbox -Ddriver_vbox=disabled
%endif

%if %{with libxl}
    %define arg_libxl -Ddriver_libxl=enabled
%else
    %define arg_libxl -Ddriver_libxl=disabled
%endif

%if %{with esx}
    %define arg_esx -Ddriver_esx=enabled -Dcurl=enabled
%else
    %define arg_esx -Ddriver_esx=disabled -Dcurl=disabled
%endif

%if %{with hyperv}
    %define arg_hyperv -Ddriver_hyperv=enabled -Dopenwsman=enabled
%else
    %define arg_hyperv -Ddriver_hyperv=disabled -Dopenwsman=disabled
%endif

%if %{with vmware}
    %define arg_vmware -Ddriver_vmware=enabled
%else
    %define arg_vmware -Ddriver_vmware=disabled
%endif

%if %{with storage_rbd}
    %define arg_storage_rbd -Dstorage_rbd=enabled
%else
    %define arg_storage_rbd -Dstorage_rbd=disabled
%endif

%if %{with storage_gluster}
    %define arg_storage_gluster -Dstorage_gluster=enabled -Dglusterfs=enabled
%else
    %define arg_storage_gluster -Dstorage_gluster=disabled -Dglusterfs=disabled
%endif

%if %{with storage_zfs}
    %define arg_storage_zfs -Dstorage_zfs=enabled
%else
    %define arg_storage_zfs -Dstorage_zfs=disabled
%endif

%if %{with numactl}
    %define arg_numactl -Dnumactl=enabled
%else
    %define arg_numactl -Dnumactl=disabled
%endif

%if %{with numad}
    %define arg_numad -Dnumad=enabled
%else
    %define arg_numad -Dnumad=disabled
%endif

%if %{with fuse}
    %define arg_fuse -Dfuse=enabled
%else
    %define arg_fuse -Dfuse=disabled
%endif

%if %{with sanlock}
    %define arg_sanlock -Dsanlock=enabled
%else
    %define arg_sanlock -Dsanlock=disabled
%endif

%if %{with firewalld_zone}
    %define arg_firewalld_zone -Dfirewalld_zone=enabled
%else
    %define arg_firewalld_zone -Dfirewalld_zone=disabled
%endif

%if %{with netcf}
    %define arg_netcf -Dnetcf=enabled
%else
    %define arg_netcf -Dnetcf=disabled
%endif

%if %{with wireshark}
    %define arg_wireshark -Dwireshark_dissector=enabled
%else
    %define arg_wireshark -Dwireshark_dissector=disabled
%endif

%if %{with storage_iscsi_direct}
    %define arg_storage_iscsi_direct -Dstorage_iscsi_direct=enabled -Dlibiscsi=enabled
%else
    %define arg_storage_iscsi_direct -Dstorage_iscsi_direct=disabled -Dlibiscsi=disabled
%endif

%if %{with libssh}
    %define arg_libssh -Dlibssh=enabled
%else
    %define arg_libssh -Dlibssh=disabled
%endif

%if %{with libssh2}
    %define arg_libssh2 -Dlibssh2=enabled
%else
    %define arg_libssh2 -Dlibssh2=disabled
%endif

%if %{with modular_daemons}
    %define arg_remote_mode -Dremote_default_mode=direct
%else
    %define arg_remote_mode -Dremote_default_mode=legacy
%endif

%define when  %(date +"%%F-%%T")
%define where %(hostname)
%define who   %{?packager}%{!?packager:Unknown}
%define arg_packager -Dpackager="%{who}, %{when}, %{where}"
%define arg_packager_version -Dpackager_version="%{release}"

%define arg_selinux_mount -Dselinux_mount="/sys/fs/selinux"

export SOURCE_DATE_EPOCH=$(stat --printf='%Y' %{_specdir}/%{name}.spec)

%meson \
           -Drunstatedir=%{_rundir} \
           %{?arg_qemu} \
           %{?arg_openvz} \
           %{?arg_lxc} \
           %{?arg_vbox} \
           %{?arg_libxl} \
           -Dsasl=enabled \
           -Dpolkit=enabled \
           -Ddriver_libvirtd=enabled \
           -Ddriver_remote=enabled \
           -Ddriver_test=enabled \
           %{?arg_esx} \
           %{?arg_hyperv} \
           %{?arg_vmware} \
           -Ddriver_vz=disabled \
           -Ddriver_bhyve=disabled \
           -Ddriver_ch=disabled \
           %{?arg_remote_mode} \
           -Ddriver_interface=enabled \
           -Ddriver_network=enabled \
           -Dstorage_fs=enabled \
           -Dstorage_lvm=enabled \
           -Dstorage_iscsi=enabled \
           %{?arg_storage_iscsi_direct} \
           -Dstorage_scsi=enabled \
           -Dstorage_disk=enabled \
           -Dstorage_mpath=enabled \
           %{?arg_storage_rbd} \
           %{?arg_storage_gluster} \
           %{?arg_storage_zfs} \
           -Dstorage_vstorage=disabled \
           %{?arg_numactl} \
           %{?arg_numad} \
           -Dcapng=enabled \
           %{?arg_fuse} \
           %{?arg_netcf} \
           -Dselinux=enabled \
           %{?arg_selinux_mount} \
           -Dapparmor=disabled \
           -Dapparmor_profiles=disabled \
           -Dsecdriver_apparmor=disabled \
           -Dudev=enabled \
           -Dyajl=enabled \
           %{?arg_sanlock} \
           -Dlibpcap=enabled \
           -Dlibnl=enabled \
           -Daudit=enabled \
           -Ddtrace=enabled \
           -Dfirewalld=enabled \
           %{?arg_firewalld_zone} \
           %{?arg_wireshark} \
           %{?arg_libssh} \
           %{?arg_libssh2} \
           -Dpm_utils=disabled \
           -Dnss=enabled \
           %{arg_packager} \
           %{arg_packager_version} \
           -Dqemu_user=%{qemu_user} \
           -Dqemu_group=%{qemu_group} \
           -Dqemu_moddir=%{qemu_moddir} \
           -Dqemu_datadir=%{qemu_datadir} \
           -Dtls_priority=%{tls_priority} \
           %{?enable_werror} \
           -Dexpensive_tests=enabled \
           -Dinit_script=systemd \
           -Ddocs=enabled \
           -Dtests=enabled \
           -Drpath=disabled \
           %{?arg_login_shell}

%meson_build

%if %{with mingw}
%mingw_meson \
  --auto-features=enabled \
  -Ddriver_remote=enabled \
  -Ddriver_test=enabled \
  -Ddriver_esx=enabled \
  -Dcurl=enabled \
  -Ddocs=enabled \
  -Dapparmor=disabled \
  -Dapparmor_profiles=disabled \
  -Dattr=disabled \
  -Daudit=disabled \
  -Dbash_completion=disabled \
  -Dblkid=disabled \
  -Dcapng=disabled \
  -Ddriver_bhyve=disabled \
  -Ddriver_hyperv=disabled \
  -Ddriver_interface=disabled \
  -Ddriver_libvirtd=disabled \
  -Ddriver_libxl=disabled \
  -Ddriver_lxc=disabled \
  -Ddriver_network=disabled \
  -Ddriver_openvz=disabled \
  -Ddriver_qemu=disabled \
  -Ddriver_secrets=disabled \
  -Ddriver_vbox=disabled \
  -Ddriver_vmware=disabled \
  -Ddriver_vz=disabled \
  -Ddtrace=disabled \
  -Dexpensive_tests=enabled \
  -Dfirewalld=disabled \
  -Dfirewalld_zone=disabled \
  -Dfuse=disabled \
  -Dglusterfs=disabled \
  -Dhost_validate=disabled \
  -Dlibiscsi=disabled \
  -Dlibnl=disabled \
  -Dlibpcap=disabled \
  -Dlibssh2=disabled \
  -Dlibssh=disabled \
  -Dlogin_shell=disabled \
  -Dnetcf=disabled \
  -Dnls=disabled \
  -Dnss=disabled \
  -Dnumactl=disabled \
  -Dnumad=disabled \
  -Dopenwsman=disabled \
  -Dpciaccess=disabled \
  -Dpm_utils=disabled \
  -Dpolkit=disabled \
  -Dreadline=disabled \
  -Drpath=disabled \
  -Dsanlock=disabled \
  -Dsasl=disabled \
  -Dsecdriver_apparmor=disabled \
  -Dsecdriver_selinux=disabled \
  -Dselinux=disabled \
  -Dstorage_dir=disabled \
  -Dstorage_disk=disabled \
  -Dstorage_fs=disabled \
  -Dstorage_gluster=disabled \
  -Dstorage_iscsi_direct=disabled \
  -Dstorage_iscsi=disabled \
  -Dstorage_lvm=disabled \
  -Dstorage_mpath=disabled \
  -Dstorage_rbd=disabled \
  -Dstorage_scsi=disabled \
  -Dstorage_vstorage=disabled \
  -Dstorage_zfs=disabled \
  -Dsysctl_config=disabled \
  -Dtests=disabled \
  -Dudev=disabled \
  -Dwireshark_dissector=disabled \
  -Dyajl=disabled
%mingw_ninja
%endif

%install
rm -fr %{buildroot}
export SOURCE_DATE_EPOCH=$(stat --printf='%Y' %{_specdir}/%{name}.spec)
%meson_install
install -d -m 0755 %{buildroot}%{_datadir}/libvirt/networks/
cp %{buildroot}%{_sysconfdir}/libvirt/qemu/networks/default.xml \
   %{buildroot}%{_datadir}/libvirt/networks/default.xml
chmod 0600 %{buildroot}%{_sysconfdir}/libvirt/qemu/networks/default.xml

install -d -m 0755 %{buildroot}%{_datadir}/libvirt/nwfilter/
cp -a %{buildroot}%{_sysconfdir}/libvirt/nwfilter/*.xml \
    %{buildroot}%{_datadir}/libvirt/nwfilter/
chmod 600 %{buildroot}%{_sysconfdir}/libvirt/nwfilter/*.xml

%if ! %{with qemu}
rm -f %{buildroot}%{_datadir}/augeas/lenses/libvirtd_qemu.aug
rm -f %{buildroot}%{_datadir}/augeas/lenses/tests/test_libvirtd_qemu.aug
%endif
%find_lang %{name}

%if ! %{with sanlock}
rm -f %{buildroot}%{_datadir}/augeas/lenses/libvirt_sanlock.aug
rm -f %{buildroot}%{_datadir}/augeas/lenses/tests/test_libvirt_sanlock.aug
%endif

%if ! %{with lxc}
rm -f %{buildroot}%{_datadir}/augeas/lenses/libvirtd_lxc.aug
rm -f %{buildroot}%{_datadir}/augeas/lenses/tests/test_libvirtd_lxc.aug
%endif

%if ! %{with qemu}
rm -rf %{buildroot}%{_sysconfdir}/libvirt/qemu.conf
rm -rf %{buildroot}%{_sysconfdir}/logrotate.d/libvirtd.qemu
%endif
%if ! %{with lxc}
rm -rf %{buildroot}%{_sysconfdir}/libvirt/lxc.conf
rm -rf %{buildroot}%{_sysconfdir}/logrotate.d/libvirtd.lxc
%endif
%if ! %{with libxl}
rm -rf %{buildroot}%{_sysconfdir}/libvirt/libxl.conf
rm -rf %{buildroot}%{_sysconfdir}/logrotate.d/libvirtd.libxl
rm -f %{buildroot}%{_datadir}/augeas/lenses/libvirtd_libxl.aug
rm -f %{buildroot}%{_datadir}/augeas/lenses/tests/test_libvirtd_libxl.aug
%endif

mv %{buildroot}%{_datadir}/doc/libvirt libvirt-docs

%ifarch %{arches_systemtap_64bit}
mv %{buildroot}%{_datadir}/systemtap/tapset/libvirt_probes.stp \
   %{buildroot}%{_datadir}/systemtap/tapset/libvirt_probes-64.stp

    %if %{with qemu}
mv %{buildroot}%{_datadir}/systemtap/tapset/libvirt_qemu_probes.stp \
   %{buildroot}%{_datadir}/systemtap/tapset/libvirt_qemu_probes-64.stp
    %endif
%endif

%if %{with mingw}
%mingw_ninja_install

rm -rf %{buildroot}%{mingw32_sysconfdir}/libvirt/nwfilter
rm -rf %{buildroot}%{mingw64_sysconfdir}/libvirt/nwfilter
rm -rf %{buildroot}%{mingw32_datadir}/doc/*
rm -rf %{buildroot}%{mingw64_datadir}/doc/*
rm -rf %{buildroot}%{mingw32_datadir}/gtk-doc/*
rm -rf %{buildroot}%{mingw64_datadir}/gtk-doc/*

rm -rf %{buildroot}%{mingw32_libexecdir}/libvirt_iohelper.exe
rm -rf %{buildroot}%{mingw64_libexecdir}/libvirt_iohelper.exe
rm -rf %{buildroot}%{mingw32_libexecdir}/libvirt-guests.sh
rm -rf %{buildroot}%{mingw64_libexecdir}/libvirt-guests.sh

%mingw_debug_install_post

%endif


%check
VIR_TEST_DEBUG=1 %meson_test --no-suite syntax-check --timeout-multiplier 10

%define libvirt_daemon_schedule_restart() mkdir -p %{_localstatedir}/lib/rpm-state/libvirt || : \
/bin/systemctl is-active %1.service 1>/dev/null 2>&1 && \
  touch %{_localstatedir}/lib/rpm-state/libvirt/restart-%1 || :

%define libvirt_daemon_finish_restart() rm -f %{_localstatedir}/lib/rpm-state/libvirt/restart-%1 \
rmdir %{_localstatedir}/lib/rpm-state/libvirt 2>/dev/null || :

%define libvirt_daemon_needs_restart() -f %{_localstatedir}/lib/rpm-state/libvirt/restart-%1

%define libvirt_daemon_perform_restart() if test %libvirt_daemon_needs_restart %1 \
then \
  /bin/systemctl try-restart %1.service >/dev/null 2>&1 || : \
fi \
%libvirt_daemon_finish_restart %1

%define libvirt_daemon_systemd_post() %systemd_post %1.socket %1-ro.socket %1-admin.socket %1.service
%define libvirt_daemon_systemd_preun() %systemd_preun %1.service %1-ro.socket %1-admin.socket %1.socket

%define libvirt_daemon_systemd_post_inet() %systemd_post %1.socket %1-ro.socket %1-admin.socket %1-tls.socket %1-tcp.socket %1.service
%define libvirt_daemon_systemd_preun_inet() %systemd_preun %1.service %1-ro.socket %1-admin.socket %1-tls.socket %1-tcp.socket %1.socket

%define libvirt_daemon_systemd_post_priv() %systemd_post %1.socket %1-admin.socket %1.service
%define libvirt_daemon_systemd_preun_priv() %systemd_preun %1.service %1-admin.socket %1.socket

%pre daemon
%libvirt_sysconfig_pre libvirtd virtproxyd virtlogd virtlockd libvirt-guests
getent group libvirt >/dev/null || groupadd -r libvirt

exit 0

%post daemon
%libvirt_daemon_systemd_post_priv virtlogd
%libvirt_daemon_systemd_post_priv virtlockd
%if %{with modular_daemons}
%libvirt_daemon_systemd_post_inet virtproxyd
%else
%libvirt_daemon_systemd_post_inet libvirtd
%endif

%systemd_post libvirt-guests.service

%libvirt_daemon_schedule_restart libvirtd

%preun daemon
%systemd_preun libvirt-guests.service

%libvirt_daemon_systemd_preun_inet libvirtd
%libvirt_daemon_systemd_preun_inet virtproxyd
%libvirt_daemon_systemd_preun_priv virtlogd
%libvirt_daemon_systemd_preun_priv virtlockd

%postun daemon
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    /bin/systemctl reload-or-try-restart virtlockd.service virtlogd.service >/dev/null 2>&1 || :
fi
%systemd_postun libvirt-guests.service

%triggerpostun daemon -- libvirt-daemon < 1.3.0
if [ $1 -ge 1 ] ; then
    /bin/systemctl is-enabled libvirtd.service 1>/dev/null 2>&1 &&
        /bin/systemctl enable virtlogd.socket virtlogd-admin.socket || :
    /bin/systemctl is-active libvirtd.service 1>/dev/null 2>&1 &&
        /bin/systemctl start virtlogd.socket virtlogd-admin.socket || :
fi

%posttrans daemon
%libvirt_sysconfig_posttrans libvirtd virtproxyd virtlogd virtlockd libvirt-guests
if test %libvirt_daemon_needs_restart libvirtd
then
    grep -E '^LIBVIRTD_ARGS=.*--listen' /etc/sysconfig/libvirtd 1>/dev/null 2>&1
    if test $? = 0
    then
        /bin/systemctl mask \
                libvirtd.socket \
                libvirtd-ro.socket \
                libvirtd-admin.socket \
                libvirtd-tls.socket \
                libvirtd-tcp.socket >/dev/null 2>&1 || :
    else
        /bin/systemctl stop libvirtd.service >/dev/null 2>&1 || :
        /bin/systemctl try-restart \
                libvirtd.socket \
                libvirtd-ro.socket \
                libvirtd-admin.socket >/dev/null 2>&1 || :
        /bin/systemctl start libvirtd.service >/dev/null 2>&1 || :
    fi
fi

%libvirt_daemon_finish_restart libvirtd

%pre daemon-driver-network
%libvirt_sysconfig_pre virtnetworkd

%post daemon-driver-network
%if %{with firewalld_zone}
    %firewalld_reload
%endif

%if %{with modular_daemons}
%libvirt_daemon_systemd_post virtnetworkd
%endif
%libvirt_daemon_schedule_restart virtnetworkd

%preun daemon-driver-network
%libvirt_daemon_systemd_preun virtnetworkd

%postun daemon-driver-network
%if %{with firewalld_zone}
    %firewalld_reload
%endif

%posttrans daemon-driver-network
%libvirt_sysconfig_posttrans virtnetworkd
%libvirt_daemon_perform_restart virtnetworkd

%pre daemon-driver-nwfilter
%libvirt_sysconfig_pre virtnwfilterd

%post daemon-driver-nwfilter
%if %{with modular_daemons}
%libvirt_daemon_systemd_post virtnwfilterd
%endif
%libvirt_daemon_schedule_restart virtnwfilterd

%preun daemon-driver-nwfilter
%libvirt_daemon_systemd_preun virtnwfilterd

%posttrans daemon-driver-nwfilter
%libvirt_sysconfig_posttrans virtnwfilterd
%libvirt_daemon_perform_restart virtnwfilterd

%pre daemon-driver-nodedev
%libvirt_sysconfig_pre virtnodedevd

%post daemon-driver-nodedev
%if %{with modular_daemons}
%libvirt_daemon_systemd_post virtnodedevd
%endif
%libvirt_daemon_schedule_restart virtnodedevd

%preun daemon-driver-nodedev
%libvirt_daemon_systemd_preun virtnodedevd

%posttrans daemon-driver-nodedev
%libvirt_sysconfig_posttrans virtnodedevd
%libvirt_daemon_perform_restart virtnodedevd

%pre daemon-driver-interface
%libvirt_sysconfig_pre virtinterfaced

%post daemon-driver-interface
%if %{with modular_daemons}
%libvirt_daemon_systemd_post virtinterfaced
%endif
%libvirt_daemon_schedule_restart virtinterfaced

%preun daemon-driver-interface
%libvirt_daemon_systemd_preun virtinterfaced

%posttrans daemon-driver-interface
%libvirt_sysconfig_posttrans virtinterfaced
%libvirt_daemon_perform_restart virtinterfaced

%pre daemon-driver-secret
%libvirt_sysconfig_pre virtsecretd

%post daemon-driver-secret
%if %{with modular_daemons}
%libvirt_daemon_systemd_post virtsecretd
%endif
%libvirt_daemon_schedule_restart virtsecretd

%preun daemon-driver-secret
%libvirt_daemon_systemd_preun virtsecretd

%posttrans daemon-driver-secret
%libvirt_sysconfig_posttrans virtsecretd
%libvirt_daemon_perform_restart virtsecretd


%pre daemon-driver-storage-core
%libvirt_sysconfig_pre virtstoraged

%post daemon-driver-storage-core
%if %{with modular_daemons}
%libvirt_daemon_systemd_post virtstoraged
%endif
%libvirt_daemon_schedule_restart virtstoraged

%preun daemon-driver-storage-core
%libvirt_daemon_systemd_preun virtstoraged

%posttrans daemon-driver-storage-core
%libvirt_sysconfig_posttrans virtstoraged
%libvirt_daemon_perform_restart virtstoraged


%if %{with qemu}
%pre daemon-driver-qemu
%libvirt_sysconfig_pre virtqemud
getent group kvm >/dev/null || groupadd -f -g 36 -r kvm
getent group qemu >/dev/null || groupadd -f -g 107 -r qemu
if ! getent passwd qemu >/dev/null; then
  if ! getent passwd 107 >/dev/null; then
    useradd -r -u 107 -g qemu -G kvm -d / -s /sbin/nologin -c "qemu user" qemu
  else
    useradd -r -g qemu -G kvm -d / -s /sbin/nologin -c "qemu user" qemu
  fi
fi
exit 0

%post daemon-driver-qemu
%if %{with modular_daemons}
%libvirt_daemon_systemd_post virtqemud
%endif
%libvirt_daemon_schedule_restart virtqemud

%preun daemon-driver-qemu
%libvirt_daemon_systemd_preun virtqemud

%posttrans daemon-driver-qemu
%libvirt_sysconfig_posttrans virtqemud
%libvirt_daemon_perform_restart virtqemud
%endif


%if %{with lxc}
%pre daemon-driver-lxc
%libvirt_sysconfig_pre virtlxcd

%post daemon-driver-lxc
%if %{with modular_daemons}
%libvirt_daemon_systemd_post virtlxcd
%endif
%libvirt_daemon_schedule_restart virtlxcd

%preun daemon-driver-lxc
%libvirt_daemon_systemd_preun virtlxcd

%posttrans daemon-driver-lxc
%libvirt_sysconfig_posttrans virtlxcd
%libvirt_daemon_perform_restart virtlxcd
%endif


%if %{with vbox}
%post daemon-driver-vbox
%if %{with modular_daemons}
%libvirt_daemon_systemd_post virtvboxd
%endif
%libvirt_daemon_schedule_restart virtvboxd

%pre daemon-driver-vbox
%libvirt_sysconfig_pre virtvboxd

%preun daemon-driver-vbox
%libvirt_daemon_systemd_preun virtvboxd

%posttrans daemon-driver-vbox
%libvirt_sysconfig_posttrans virtvboxd
%libvirt_daemon_perform_restart virtvboxd
%endif


%if %{with libxl}
%post daemon-driver-libxl
%if %{with modular_daemons}
%libvirt_daemon_systemd_post virtxend
%endif
%libvirt_daemon_schedule_restart virtxend

%pre daemon-driver-libxl
%libvirt_sysconfig_pre virtxend

%preun daemon-driver-libxl
%libvirt_daemon_systemd_preun virtxend

%posttrans daemon-driver-libxl
%libvirt_sysconfig_posttrans virtxend
%libvirt_daemon_perform_restart virtxend
%endif


%post daemon-config-network
if test $1 -eq 1 && test ! -f %{_sysconfdir}/libvirt/qemu/networks/default.xml ; then
    orig_sub=122
    sub=${orig_sub}
    nl='
'
    routes="${nl}$(ip route show | cut -d' ' -f1)${nl}"
    case ${routes} in
      *"${nl}192.168.${orig_sub}.0/24${nl}"*)
        for new_sub in $(seq 124 254); do
          case ${routes} in
          *"${nl}192.168.${new_sub}.0/24${nl}"*)
            ;;
          *)
            sub=$new_sub
            break;
            ;;
          esac
        done
        ;;
      *)
        ;;
    esac

    sed -e "s/${orig_sub}/${sub}/g" \
         < %{_datadir}/libvirt/networks/default.xml \
         > %{_sysconfdir}/libvirt/qemu/networks/default.xml
    ln -s ../default.xml %{_sysconfdir}/libvirt/qemu/networks/autostart/default.xml
    chmod 0600 %{_sysconfdir}/libvirt/qemu/networks/default.xml
    %libvirt_daemon_schedule_restart libvirtd
    %libvirt_daemon_schedule_restart virtnetworkd
fi

%posttrans daemon-config-network
%libvirt_daemon_perform_restart libvirtd
%libvirt_daemon_perform_restart virtnetworkd

%post daemon-config-nwfilter
for datadir_file in %{_datadir}/libvirt/nwfilter/*.xml; do
  sysconfdir_file=%{_sysconfdir}/libvirt/nwfilter/$(basename "$datadir_file")
  if [ ! -f "$sysconfdir_file" ]; then
    install -m 0600 "$datadir_file" "$sysconfdir_file"
  fi
done
%libvirt_daemon_schedule_restart libvirtd
%libvirt_daemon_schedule_restart virtnwfilterd

%posttrans daemon-config-nwfilter
%libvirt_daemon_perform_restart libvirtd
%libvirt_daemon_perform_restart virtnwfilterd

%if %{with lxc}
%pre login-shell
getent group virtlogin >/dev/null || groupadd -r virtlogin
exit 0
%endif

%files

%files docs
%doc AUTHORS.rst NEWS.rst README.rst
%doc libvirt-docs/*

%files daemon

%{_unitdir}/libvirtd.service
%{_unitdir}/libvirtd.socket
%{_unitdir}/libvirtd-ro.socket
%{_unitdir}/libvirtd-admin.socket
%{_unitdir}/libvirtd-tcp.socket
%{_unitdir}/libvirtd-tls.socket
%{_unitdir}/virtproxyd.service
%{_unitdir}/virtproxyd.socket
%{_unitdir}/virtproxyd-ro.socket
%{_unitdir}/virtproxyd-admin.socket
%{_unitdir}/virtproxyd-tcp.socket
%{_unitdir}/virtproxyd-tls.socket
%{_unitdir}/virt-guest-shutdown.target
%{_unitdir}/virtlogd.service
%{_unitdir}/virtlogd.socket
%{_unitdir}/virtlogd-admin.socket
%{_unitdir}/virtlockd.service
%{_unitdir}/virtlockd.socket
%{_unitdir}/virtlockd-admin.socket
%{_unitdir}/libvirt-guests.service
%config(noreplace) %{_sysconfdir}/libvirt/libvirtd.conf
%config(noreplace) %{_sysconfdir}/libvirt/virtproxyd.conf
%config(noreplace) %{_sysconfdir}/libvirt/virtlogd.conf
%config(noreplace) %{_sysconfdir}/libvirt/virtlockd.conf
%config(noreplace) %{_sysconfdir}/sasl2/libvirt.conf
%config(noreplace) %{_prefix}/lib/sysctl.d/60-libvirtd.conf

%config(noreplace) %{_sysconfdir}/logrotate.d/libvirtd
%dir %{_datadir}/libvirt/

%ghost %dir %{_rundir}/libvirt/
%ghost %dir %{_rundir}/libvirt/common/

%dir %attr(0755, root, root) %{_localstatedir}/lib/libvirt/
%dir %attr(0711, root, root) %{_localstatedir}/lib/libvirt/images/
%dir %attr(0711, root, root) %{_localstatedir}/lib/libvirt/filesystems/
%dir %attr(0711, root, root) %{_localstatedir}/lib/libvirt/boot/
%dir %attr(0711, root, root) %{_localstatedir}/cache/libvirt/


%dir %attr(0755, root, root) %{_libdir}/libvirt/
%dir %attr(0755, root, root) %{_libdir}/libvirt/connection-driver/
%dir %attr(0755, root, root) %{_libdir}/libvirt/lock-driver
%attr(0755, root, root) %{_libdir}/libvirt/lock-driver/lockd.so

%{_datadir}/augeas/lenses/libvirtd.aug
%{_datadir}/augeas/lenses/tests/test_libvirtd.aug
%{_datadir}/augeas/lenses/virtlogd.aug
%{_datadir}/augeas/lenses/tests/test_virtlogd.aug
%{_datadir}/augeas/lenses/virtlockd.aug
%{_datadir}/augeas/lenses/tests/test_virtlockd.aug
%{_datadir}/augeas/lenses/virtproxyd.aug
%{_datadir}/augeas/lenses/tests/test_virtproxyd.aug
%{_datadir}/augeas/lenses/libvirt_lockd.aug
%if %{with qemu}
%{_datadir}/augeas/lenses/tests/test_libvirt_lockd.aug
%endif

%{_datadir}/polkit-1/actions/org.libvirt.unix.policy
%{_datadir}/polkit-1/actions/org.libvirt.api.policy
%{_datadir}/polkit-1/rules.d/50-libvirt.rules

%dir %attr(0700, root, root) %{_localstatedir}/log/libvirt/

%attr(0755, root, root) %{_libexecdir}/libvirt_iohelper

%attr(0755, root, root) %{_bindir}/virt-ssh-helper

%attr(0755, root, root) %{_sbindir}/libvirtd
%attr(0755, root, root) %{_sbindir}/virtproxyd
%attr(0755, root, root) %{_sbindir}/virtlogd
%attr(0755, root, root) %{_sbindir}/virtlockd
%attr(0755, root, root) %{_libexecdir}/libvirt-guests.sh

%{_mandir}/man1/virt-admin.1*
%{_mandir}/man1/virt-host-validate.1*
%{_mandir}/man8/virt-ssh-helper.8*
%{_mandir}/man8/libvirt-guests.8*
%{_mandir}/man8/libvirtd.8*
%{_mandir}/man8/virtlogd.8*
%{_mandir}/man8/virtlockd.8*
%{_mandir}/man8/virtproxyd.8*

%{_bindir}/virt-host-validate
%{_bindir}/virt-admin
%{_datadir}/bash-completion/completions/virt-admin

%files daemon-config-network
%dir %{_datadir}/libvirt/networks/
%{_datadir}/libvirt/networks/default.xml
%ghost %{_sysconfdir}/libvirt/qemu/networks/default.xml
%ghost %{_sysconfdir}/libvirt/qemu/networks/autostart/default.xml

%files daemon-config-nwfilter
%dir %{_datadir}/libvirt/nwfilter/
%{_datadir}/libvirt/nwfilter/*.xml
%ghost %{_sysconfdir}/libvirt/nwfilter/*.xml

%files daemon-driver-interface
%config(noreplace) %{_sysconfdir}/libvirt/virtinterfaced.conf
%{_datadir}/augeas/lenses/virtinterfaced.aug
%{_datadir}/augeas/lenses/tests/test_virtinterfaced.aug
%{_unitdir}/virtinterfaced.service
%{_unitdir}/virtinterfaced.socket
%{_unitdir}/virtinterfaced-ro.socket
%{_unitdir}/virtinterfaced-admin.socket
%attr(0755, root, root) %{_sbindir}/virtinterfaced
%ghost %dir %{_rundir}/libvirt/interface/
%{_libdir}/%{name}/connection-driver/libvirt_driver_interface.so
%{_mandir}/man8/virtinterfaced.8*

%files daemon-driver-network
%config(noreplace) %{_sysconfdir}/libvirt/virtnetworkd.conf
%{_datadir}/augeas/lenses/virtnetworkd.aug
%{_datadir}/augeas/lenses/tests/test_virtnetworkd.aug
%{_unitdir}/virtnetworkd.service
%{_unitdir}/virtnetworkd.socket
%{_unitdir}/virtnetworkd-ro.socket
%{_unitdir}/virtnetworkd-admin.socket
%attr(0755, root, root) %{_sbindir}/virtnetworkd
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/qemu/
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/qemu/networks/
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/qemu/networks/autostart
%ghost %dir %{_rundir}/libvirt/network/
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/network/
%dir %attr(0755, root, root) %{_localstatedir}/lib/libvirt/dnsmasq/
%attr(0755, root, root) %{_libexecdir}/libvirt_leaseshelper
%{_libdir}/%{name}/connection-driver/libvirt_driver_network.so
%{_mandir}/man8/virtnetworkd.8*

%if %{with firewalld_zone}
%{_prefix}/lib/firewalld/zones/libvirt.xml
%{_prefix}/lib/firewalld/zones/libvirt-routed.xml
%{_prefix}/lib/firewalld/policies/libvirt-routed-in.xml
%{_prefix}/lib/firewalld/policies/libvirt-routed-out.xml
%{_prefix}/lib/firewalld/policies/libvirt-to-host.xml
%endif

%files daemon-driver-nodedev
%config(noreplace) %{_sysconfdir}/libvirt/virtnodedevd.conf
%{_datadir}/augeas/lenses/virtnodedevd.aug
%{_datadir}/augeas/lenses/tests/test_virtnodedevd.aug
%{_unitdir}/virtnodedevd.service
%{_unitdir}/virtnodedevd.socket
%{_unitdir}/virtnodedevd-ro.socket
%{_unitdir}/virtnodedevd-admin.socket
%attr(0755, root, root) %{_sbindir}/virtnodedevd
%ghost %dir %{_rundir}/libvirt/nodedev/
%{_libdir}/%{name}/connection-driver/libvirt_driver_nodedev.so
%{_mandir}/man8/virtnodedevd.8*

%files daemon-driver-nwfilter
%config(noreplace) %{_sysconfdir}/libvirt/virtnwfilterd.conf
%{_datadir}/augeas/lenses/virtnwfilterd.aug
%{_datadir}/augeas/lenses/tests/test_virtnwfilterd.aug
%{_unitdir}/virtnwfilterd.service
%{_unitdir}/virtnwfilterd.socket
%{_unitdir}/virtnwfilterd-ro.socket
%{_unitdir}/virtnwfilterd-admin.socket
%attr(0755, root, root) %{_sbindir}/virtnwfilterd
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/nwfilter/
%ghost %dir %{_rundir}/libvirt/network/
%ghost %dir %{_rundir}/libvirt/nwfilter-binding/
%ghost %dir %{_rundir}/libvirt/nwfilter/
%{_libdir}/%{name}/connection-driver/libvirt_driver_nwfilter.so
%{_mandir}/man8/virtnwfilterd.8*

%files daemon-driver-secret
%config(noreplace) %{_sysconfdir}/libvirt/virtsecretd.conf
%{_datadir}/augeas/lenses/virtsecretd.aug
%{_datadir}/augeas/lenses/tests/test_virtsecretd.aug
%{_unitdir}/virtsecretd.service
%{_unitdir}/virtsecretd.socket
%{_unitdir}/virtsecretd-ro.socket
%{_unitdir}/virtsecretd-admin.socket
%attr(0755, root, root) %{_sbindir}/virtsecretd
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/secrets/
%ghost %dir %{_rundir}/libvirt/secrets/
%{_libdir}/%{name}/connection-driver/libvirt_driver_secret.so
%{_mandir}/man8/virtsecretd.8*

%files daemon-driver-storage

%files daemon-driver-storage-core
%config(noreplace) %{_sysconfdir}/libvirt/virtstoraged.conf
%{_datadir}/augeas/lenses/virtstoraged.aug
%{_datadir}/augeas/lenses/tests/test_virtstoraged.aug
%{_unitdir}/virtstoraged.service
%{_unitdir}/virtstoraged.socket
%{_unitdir}/virtstoraged-ro.socket
%{_unitdir}/virtstoraged-admin.socket
%attr(0755, root, root) %{_sbindir}/virtstoraged
%attr(0755, root, root) %{_libexecdir}/libvirt_parthelper
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/storage/
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/storage/autostart/
%ghost %dir %{_rundir}/libvirt/storage/
%{_libdir}/%{name}/connection-driver/libvirt_driver_storage.so
%{_libdir}/%{name}/storage-backend/libvirt_storage_backend_fs.so
%{_libdir}/%{name}/storage-file/libvirt_storage_file_fs.so
%{_mandir}/man8/virtstoraged.8*

%files daemon-driver-storage-disk
%{_libdir}/%{name}/storage-backend/libvirt_storage_backend_disk.so

%files daemon-driver-storage-logical
%{_libdir}/%{name}/storage-backend/libvirt_storage_backend_logical.so

%files daemon-driver-storage-scsi
%{_libdir}/%{name}/storage-backend/libvirt_storage_backend_scsi.so

%files daemon-driver-storage-iscsi
%{_libdir}/%{name}/storage-backend/libvirt_storage_backend_iscsi.so

%if %{with storage_iscsi_direct}
%files daemon-driver-storage-iscsi-direct
%{_libdir}/%{name}/storage-backend/libvirt_storage_backend_iscsi-direct.so
%endif

%files daemon-driver-storage-mpath
%{_libdir}/%{name}/storage-backend/libvirt_storage_backend_mpath.so

%if %{with storage_gluster}
%files daemon-driver-storage-gluster
%{_libdir}/%{name}/storage-backend/libvirt_storage_backend_gluster.so
%{_libdir}/%{name}/storage-file/libvirt_storage_file_gluster.so
%endif

%if %{with storage_rbd}
%files daemon-driver-storage-rbd
%{_libdir}/%{name}/storage-backend/libvirt_storage_backend_rbd.so
%endif

%if %{with storage_zfs}
%files daemon-driver-storage-zfs
%{_libdir}/%{name}/storage-backend/libvirt_storage_backend_zfs.so
%endif

%if %{with qemu}
%files daemon-driver-qemu
%config(noreplace) %{_sysconfdir}/libvirt/virtqemud.conf
%config(noreplace) %{_prefix}/lib/sysctl.d/60-qemu-postcopy-migration.conf
%{_datadir}/augeas/lenses/virtqemud.aug
%{_datadir}/augeas/lenses/tests/test_virtqemud.aug
%{_unitdir}/virtqemud.service
%{_unitdir}/virtqemud.socket
%{_unitdir}/virtqemud-ro.socket
%{_unitdir}/virtqemud-admin.socket
%attr(0755, root, root) %{_sbindir}/virtqemud
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/qemu/
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/qemu/autostart/
%dir %attr(0700, root, root) %{_localstatedir}/log/libvirt/qemu/
%config(noreplace) %{_sysconfdir}/libvirt/qemu.conf
%config(noreplace) %{_sysconfdir}/libvirt/qemu-lockd.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/libvirtd.qemu
%ghost %dir %{_rundir}/libvirt/qemu/
%ghost %dir %{_rundir}/libvirt/qemu/dbus/
%ghost %dir %{_rundir}/libvirt/qemu/slirp/
%ghost %dir %{_rundir}/libvirt/qemu/swtpm/
%dir %attr(0751, %{qemu_user}, %{qemu_group}) %{_localstatedir}/lib/libvirt/qemu/
%dir %attr(0751, %{qemu_user}, %{qemu_group}) %{_localstatedir}/lib/libvirt/qemu/channel/
%dir %attr(0751, %{qemu_user}, %{qemu_group}) %{_localstatedir}/lib/libvirt/qemu/channel/target/
%dir %attr(0751, %{qemu_user}, %{qemu_group}) %{_localstatedir}/lib/libvirt/qemu/checkpoint/
%dir %attr(0751, %{qemu_user}, %{qemu_group}) %{_localstatedir}/lib/libvirt/qemu/dump/
%dir %attr(0751, %{qemu_user}, %{qemu_group}) %{_localstatedir}/lib/libvirt/qemu/nvram/
%dir %attr(0751, %{qemu_user}, %{qemu_group}) %{_localstatedir}/lib/libvirt/qemu/ram/
%dir %attr(0751, %{qemu_user}, %{qemu_group}) %{_localstatedir}/lib/libvirt/qemu/save/
%dir %attr(0751, %{qemu_user}, %{qemu_group}) %{_localstatedir}/lib/libvirt/qemu/snapshot/
%dir %attr(0750, root, root) %{_localstatedir}/cache/libvirt/qemu/
%{_datadir}/augeas/lenses/libvirtd_qemu.aug
%{_datadir}/augeas/lenses/tests/test_libvirtd_qemu.aug
%{_libdir}/%{name}/connection-driver/libvirt_driver_qemu.so
%dir %attr(0711, root, root) %{_localstatedir}/lib/libvirt/swtpm/
%dir %attr(0730, tss, tss) %{_localstatedir}/log/swtpm/libvirt/qemu/
%{_bindir}/virt-qemu-run
%{_mandir}/man1/virt-qemu-run.1*
%{_mandir}/man8/virtqemud.8*
%endif

%if %{with lxc}
%files daemon-driver-lxc
%config(noreplace) %{_sysconfdir}/libvirt/virtlxcd.conf
%{_datadir}/augeas/lenses/virtlxcd.aug
%{_datadir}/augeas/lenses/tests/test_virtlxcd.aug
%{_unitdir}/virtlxcd.service
%{_unitdir}/virtlxcd.socket
%{_unitdir}/virtlxcd-ro.socket
%{_unitdir}/virtlxcd-admin.socket
%attr(0755, root, root) %{_sbindir}/virtlxcd
%dir %attr(0700, root, root) %{_localstatedir}/log/libvirt/lxc/
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/lxc/
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/lxc/autostart/
%config(noreplace) %{_sysconfdir}/libvirt/lxc.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/libvirtd.lxc
%ghost %dir %{_rundir}/libvirt/lxc/
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/lxc/
%{_datadir}/augeas/lenses/libvirtd_lxc.aug
%{_datadir}/augeas/lenses/tests/test_libvirtd_lxc.aug
%attr(0755, root, root) %{_libexecdir}/libvirt_lxc
%{_libdir}/%{name}/connection-driver/libvirt_driver_lxc.so
%{_mandir}/man8/virtlxcd.8*
%endif

%if %{with libxl}
%files daemon-driver-libxl
%config(noreplace) %{_sysconfdir}/libvirt/virtxend.conf
%{_datadir}/augeas/lenses/virtxend.aug
%{_datadir}/augeas/lenses/tests/test_virtxend.aug
%{_unitdir}/virtxend.service
%{_unitdir}/virtxend.socket
%{_unitdir}/virtxend-ro.socket
%{_unitdir}/virtxend-admin.socket
%attr(0755, root, root) %{_sbindir}/virtxend
%config(noreplace) %{_sysconfdir}/libvirt/libxl.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/libvirtd.libxl
%config(noreplace) %{_sysconfdir}/libvirt/libxl-lockd.conf
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/libxl/
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/libxl/autostart/
%{_datadir}/augeas/lenses/libvirtd_libxl.aug
%{_datadir}/augeas/lenses/tests/test_libvirtd_libxl.aug
%dir %attr(0700, root, root) %{_localstatedir}/log/libvirt/libxl/
%ghost %dir %{_rundir}/libvirt/libxl/
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/libxl/
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/libxl/channel/
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/libxl/channel/target/
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/libxl/dump/
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/libxl/save/
%{_libdir}/%{name}/connection-driver/libvirt_driver_libxl.so
%{_mandir}/man8/virtxend.8*
%endif

%if %{with vbox}
%files daemon-driver-vbox
%config(noreplace) %{_sysconfdir}/libvirt/virtvboxd.conf
%{_datadir}/augeas/lenses/virtvboxd.aug
%{_datadir}/augeas/lenses/tests/test_virtvboxd.aug
%{_unitdir}/virtvboxd.service
%{_unitdir}/virtvboxd.socket
%{_unitdir}/virtvboxd-ro.socket
%{_unitdir}/virtvboxd-admin.socket
%attr(0755, root, root) %{_sbindir}/virtvboxd
%{_libdir}/%{name}/connection-driver/libvirt_driver_vbox.so
%{_mandir}/man8/virtvboxd.8*
%endif

%if %{with qemu_tcg}
%files daemon-qemu
%endif

%if %{with qemu_kvm}
%files daemon-kvm
%endif

%if %{with lxc}
%files daemon-lxc
%endif

%if %{with libxl}
%files daemon-xen
%endif

%if %{with vbox}
%files daemon-vbox
%endif

%if %{with sanlock}
%files lock-sanlock
    %if %{with qemu}
%config(noreplace) %{_sysconfdir}/libvirt/qemu-sanlock.conf
    %endif
    %if %{with libxl}
%config(noreplace) %{_sysconfdir}/libvirt/libxl-sanlock.conf
    %endif
%attr(0755, root, root) %{_libdir}/libvirt/lock-driver/sanlock.so
%{_datadir}/augeas/lenses/libvirt_sanlock.aug
%{_datadir}/augeas/lenses/tests/test_libvirt_sanlock.aug
%dir %attr(0770, root, sanlock) %{_localstatedir}/lib/libvirt/sanlock
%{_sbindir}/virt-sanlock-cleanup
%{_mandir}/man8/virt-sanlock-cleanup.8*
%attr(0755, root, root) %{_libexecdir}/libvirt_sanlock_helper
%endif

%files client
%{_mandir}/man1/virsh.1*
%{_mandir}/man1/virt-xml-validate.1*
%{_mandir}/man1/virt-pki-query-dn.1*
%{_mandir}/man1/virt-pki-validate.1*
%{_mandir}/man7/virkey*.7*
%{_bindir}/virsh
%{_bindir}/virt-xml-validate
%{_bindir}/virt-pki-query-dn
%{_bindir}/virt-pki-validate

%{_datadir}/bash-completion/completions/virsh

%if %{with qemu}
%files client-qemu
%{_mandir}/man1/virt-qemu-qmp-proxy.1*
%{_mandir}/man1/virt-qemu-sev-validate.1*
%{_bindir}/virt-qemu-qmp-proxy
%{_bindir}/virt-qemu-sev-validate
%endif

%files libs -f %{name}.lang
%license COPYING COPYING.LESSER
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/
%config(noreplace) %{_sysconfdir}/libvirt/libvirt.conf
%config(noreplace) %{_sysconfdir}/libvirt/libvirt-admin.conf
%{_libdir}/libvirt.so.*
%{_libdir}/libvirt-qemu.so.*
%{_libdir}/libvirt-lxc.so.*
%{_libdir}/libvirt-admin.so.*
%dir %{_datadir}/libvirt/
%dir %{_datadir}/libvirt/schemas/

%{_datadir}/systemtap/tapset/libvirt_probes*.stp
%{_datadir}/systemtap/tapset/libvirt_functions.stp
%if %{with qemu}
%{_datadir}/systemtap/tapset/libvirt_qemu_probes*.stp
%endif

%{_datadir}/libvirt/schemas/*.rng

%{_datadir}/libvirt/cpu_map/*.xml

%{_datadir}/libvirt/test-screenshot.png

%if %{with wireshark}
%files wireshark
%{wireshark_plugindir}/libvirt.so
%endif

%files nss
%{_libdir}/libnss_libvirt.so.2
%{_libdir}/libnss_libvirt_guest.so.2

%if %{with lxc}
%files login-shell
%attr(4750, root, virtlogin) %{_bindir}/virt-login-shell
%{_libexecdir}/virt-login-shell-helper
%config(noreplace) %{_sysconfdir}/libvirt/virt-login-shell.conf
%{_mandir}/man1/virt-login-shell.1*
%endif

%files devel
%{_libdir}/libvirt.so
%{_libdir}/libvirt-admin.so
%{_libdir}/libvirt-qemu.so
%{_libdir}/libvirt-lxc.so
%dir %{_includedir}/libvirt
%{_includedir}/libvirt/virterror.h
%{_includedir}/libvirt/libvirt.h
%{_includedir}/libvirt/libvirt-admin.h
%{_includedir}/libvirt/libvirt-common.h
%{_includedir}/libvirt/libvirt-domain.h
%{_includedir}/libvirt/libvirt-domain-checkpoint.h
%{_includedir}/libvirt/libvirt-domain-snapshot.h
%{_includedir}/libvirt/libvirt-event.h
%{_includedir}/libvirt/libvirt-host.h
%{_includedir}/libvirt/libvirt-interface.h
%{_includedir}/libvirt/libvirt-network.h
%{_includedir}/libvirt/libvirt-nodedev.h
%{_includedir}/libvirt/libvirt-nwfilter.h
%{_includedir}/libvirt/libvirt-secret.h
%{_includedir}/libvirt/libvirt-storage.h
%{_includedir}/libvirt/libvirt-stream.h
%{_includedir}/libvirt/libvirt-qemu.h
%{_includedir}/libvirt/libvirt-lxc.h
%{_libdir}/pkgconfig/libvirt.pc
%{_libdir}/pkgconfig/libvirt-admin.pc
%{_libdir}/pkgconfig/libvirt-qemu.pc
%{_libdir}/pkgconfig/libvirt-lxc.pc

%dir %{_datadir}/libvirt/api/
%{_datadir}/libvirt/api/libvirt-api.xml
%{_datadir}/libvirt/api/libvirt-admin-api.xml
%{_datadir}/libvirt/api/libvirt-qemu-api.xml
%{_datadir}/libvirt/api/libvirt-lxc-api.xml

%if %{with mingw}
%files -n mingw32-libvirt
%dir %{mingw32_sysconfdir}/libvirt/
%config(noreplace) %{mingw32_sysconfdir}/libvirt/libvirt.conf
%config(noreplace) %{mingw32_sysconfdir}/libvirt/libvirt-admin.conf

%{mingw32_bindir}/libvirt-0.dll
%{mingw32_bindir}/virsh.exe
%{mingw32_bindir}/virt-admin.exe
%{mingw32_bindir}/virt-xml-validate
%{mingw32_bindir}/virt-pki-query-dn.exe
%{mingw32_bindir}/virt-pki-validate
%{mingw32_bindir}/libvirt-lxc-0.dll
%{mingw32_bindir}/libvirt-qemu-0.dll
%{mingw32_bindir}/libvirt-admin-0.dll

%{mingw32_libdir}/libvirt.dll.a
%{mingw32_libdir}/pkgconfig/libvirt.pc
%{mingw32_libdir}/pkgconfig/libvirt-qemu.pc
%{mingw32_libdir}/pkgconfig/libvirt-lxc.pc
%{mingw32_libdir}/pkgconfig/libvirt-admin.pc
%{mingw32_libdir}/libvirt-lxc.dll.a
%{mingw32_libdir}/libvirt-qemu.dll.a
%{mingw32_libdir}/libvirt-admin.dll.a

%dir %{mingw32_datadir}/libvirt/
%dir %{mingw32_datadir}/libvirt/schemas/
%{mingw32_datadir}/libvirt/schemas/*.rng

%dir %{mingw32_datadir}/libvirt/api/
%{mingw32_datadir}/libvirt/api/libvirt-api.xml
%{mingw32_datadir}/libvirt/api/libvirt-lxc-api.xml
%{mingw32_datadir}/libvirt/api/libvirt-qemu-api.xml
%{mingw32_datadir}/libvirt/api/libvirt-admin-api.xml

%{mingw32_datadir}/libvirt/cpu_map/*.xml

%{mingw32_datadir}/libvirt/test-screenshot.png

%{mingw32_datadir}/locale/*/LC_MESSAGES/libvirt.mo

%dir %{mingw32_includedir}/libvirt
%{mingw32_includedir}/libvirt/libvirt.h
%{mingw32_includedir}/libvirt/libvirt-common.h
%{mingw32_includedir}/libvirt/libvirt-domain.h
%{mingw32_includedir}/libvirt/libvirt-domain-checkpoint.h
%{mingw32_includedir}/libvirt/libvirt-domain-snapshot.h
%{mingw32_includedir}/libvirt/libvirt-event.h
%{mingw32_includedir}/libvirt/libvirt-host.h
%{mingw32_includedir}/libvirt/libvirt-interface.h
%{mingw32_includedir}/libvirt/libvirt-network.h
%{mingw32_includedir}/libvirt/libvirt-nodedev.h
%{mingw32_includedir}/libvirt/libvirt-nwfilter.h
%{mingw32_includedir}/libvirt/libvirt-secret.h
%{mingw32_includedir}/libvirt/libvirt-storage.h
%{mingw32_includedir}/libvirt/libvirt-stream.h
%{mingw32_includedir}/libvirt/virterror.h
%{mingw32_includedir}/libvirt/libvirt-lxc.h
%{mingw32_includedir}/libvirt/libvirt-qemu.h
%{mingw32_includedir}/libvirt/libvirt-admin.h

%{mingw32_mandir}/man1/virsh.1*
%{mingw32_mandir}/man1/virt-admin.1*
%{mingw32_mandir}/man1/virt-xml-validate.1*
%{mingw32_mandir}/man1/virt-pki-query-dn.1*
%{mingw32_mandir}/man1/virt-pki-validate.1*
%{mingw32_mandir}/man7/virkey*.7*


%files -n mingw64-libvirt
%dir %{mingw64_sysconfdir}/libvirt/
%config(noreplace) %{mingw64_sysconfdir}/libvirt/libvirt.conf
%config(noreplace) %{mingw64_sysconfdir}/libvirt/libvirt-admin.conf

%{mingw64_bindir}/libvirt-0.dll
%{mingw64_bindir}/virsh.exe
%{mingw64_bindir}/virt-admin.exe
%{mingw64_bindir}/virt-xml-validate
%{mingw64_bindir}/virt-pki-query-dn.exe
%{mingw64_bindir}/virt-pki-validate
%{mingw64_bindir}/libvirt-lxc-0.dll
%{mingw64_bindir}/libvirt-qemu-0.dll
%{mingw64_bindir}/libvirt-admin-0.dll

%{mingw64_libdir}/libvirt.dll.a
%{mingw64_libdir}/pkgconfig/libvirt.pc
%{mingw64_libdir}/pkgconfig/libvirt-qemu.pc
%{mingw64_libdir}/pkgconfig/libvirt-lxc.pc
%{mingw64_libdir}/pkgconfig/libvirt-admin.pc
%{mingw64_libdir}/libvirt-lxc.dll.a
%{mingw64_libdir}/libvirt-qemu.dll.a
%{mingw64_libdir}/libvirt-admin.dll.a

%dir %{mingw64_datadir}/libvirt/
%dir %{mingw64_datadir}/libvirt/schemas/
%{mingw64_datadir}/libvirt/schemas/*.rng

%dir %{mingw64_datadir}/libvirt/api/
%{mingw64_datadir}/libvirt/api/libvirt-api.xml
%{mingw64_datadir}/libvirt/api/libvirt-lxc-api.xml
%{mingw64_datadir}/libvirt/api/libvirt-qemu-api.xml
%{mingw64_datadir}/libvirt/api/libvirt-admin-api.xml

%{mingw64_datadir}/libvirt/cpu_map/*.xml

%{mingw64_datadir}/libvirt/test-screenshot.png

%{mingw64_datadir}/locale/*/LC_MESSAGES/libvirt.mo

%dir %{mingw64_includedir}/libvirt
%{mingw64_includedir}/libvirt/libvirt.h
%{mingw64_includedir}/libvirt/libvirt-common.h
%{mingw64_includedir}/libvirt/libvirt-domain.h
%{mingw64_includedir}/libvirt/libvirt-domain-checkpoint.h
%{mingw64_includedir}/libvirt/libvirt-domain-snapshot.h
%{mingw64_includedir}/libvirt/libvirt-event.h
%{mingw64_includedir}/libvirt/libvirt-host.h
%{mingw64_includedir}/libvirt/libvirt-interface.h
%{mingw64_includedir}/libvirt/libvirt-network.h
%{mingw64_includedir}/libvirt/libvirt-nodedev.h
%{mingw64_includedir}/libvirt/libvirt-nwfilter.h
%{mingw64_includedir}/libvirt/libvirt-secret.h
%{mingw64_includedir}/libvirt/libvirt-storage.h
%{mingw64_includedir}/libvirt/libvirt-stream.h
%{mingw64_includedir}/libvirt/virterror.h
%{mingw64_includedir}/libvirt/libvirt-lxc.h
%{mingw64_includedir}/libvirt/libvirt-qemu.h
%{mingw64_includedir}/libvirt/libvirt-admin.h

%{mingw64_mandir}/man1/virsh.1*
%{mingw64_mandir}/man1/virt-admin.1*
%{mingw64_mandir}/man1/virt-xml-validate.1*
%{mingw64_mandir}/man1/virt-pki-query-dn.1*
%{mingw64_mandir}/man1/virt-pki-validate.1*
%{mingw64_mandir}/man7/virkey*.7*
%endif


%changelog
* Thu Apr 25 2024 Fanjun Kong <fanjunkong@tencent.com> - 9.6.0-8
- Enable build with mingw for epol

* Wed Jan 10 2024 Rebuild Robot <rebot@opencloudos.org> - 9.6.0-7
- Rebuilt for iscsi-initiator-utils

* Fri Oct 20 2023 jackeyji <jackeyji@tencent.com> - 9.6.0-6
- Rebuild for qemu CVE fix

* Thu Oct 12 2023 Miaojun Dong <zoedong@tencent.com> - 9.6.0-5
- Rebuild for curl-8.4.0

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 9.6.0-4
- Rebuilt for OpenCloudOS Stream 23.09

* Thu Aug 24 2023 Shuo Wang <abushwang@tencent.com> - 9.6.0-3
- Rebuilt for sanlock 3.8.5

* Mon Aug 21 2023 Miaojun Dong <zoedong@tencent.com> - 9.6.0-2
- Rebuid for parted-3.6

* Mon Aug 14 2023 cunshunxia <cunshunxia@tencent.com> - 9.6.0-1
- upgrade to 9.6.0

* Tue Jul 18 2023 Miaojun Dong <zoedong@tencent.com> - 9.4.0-1
- Bump version to 9.4.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 8.10.0-4
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 8.10.0-3
- Rebuilt for OpenCloudOS Stream 23

* Thu Feb 23 2023 cunshunxia <cunshunxia@tencent.com> - 8.10.0-2
- change source to source0

* Mon Dec 26 2022 Feng Weiyao <wynnfeng@tencent.com> - 8.10.0-1
- package init.
